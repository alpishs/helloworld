set :application, "hello_world"
set :repo_url, "https://alpishs@bitbucket.org/alpishs/helloworld.git"
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')
set :rvm_ruby_version, 'ruby-2.6.2'
set :passenger_restart_with_touch, true
set :use_sudo, true