role :app, %w{deploy_user@3.15.149.116}
role :web, %w{deploy_user@3.15.149.116}
role :db,  %w{deploy_user@3.15.149.116}
set :ssh_options, {
    keys: %w(/Users/alpish.singhal/.ssh/id_rsa),
    forward_agent: false,
    auth_methods: %w(publickey password)
}